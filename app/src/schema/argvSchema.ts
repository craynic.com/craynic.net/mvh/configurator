import {Schema} from "jsonschema";
import * as path from "path";

export type ArgvType = {
    "mongo-conn-uri": string,
    "mongo-db": string,
    "apache-sites-dir": string,
    "php-pool-dir": string,
    "php-pool-sockets-dir": string,
    "php-pool-sockets-type": "unix" | "tcp",
    "mounted-sites-root-dir": string,
    "target-sites-root-dir": string,
    "sftp-keys-dir": string,
    "apache-gid": number,
    "watch": boolean,
    "ssl-certs-dir": string,
    "users-dir": string,
    "k8s-certificate-issuer": string,
    "apache-enabled-ports": string,
    "apache-default-site": string,
    "php-version-header-name": string,
    "target-namespace": string,
}

export type ArgvPartialType = Partial<ArgvType>

export type ArgvDescriptionItemType<T> = {
    defaultValue?: T,
    description: string,
    envVariableName: string,
};

type ArgvDescriptionType = {[index: string]: ArgvDescriptionItemType<any>} & {
    [P in keyof ArgvType]: ArgvDescriptionItemType<ArgvType[P]>
};

export const argvDescription: ArgvDescriptionType = {
    "mongo-conn-uri": {
        description: 'Connection URI to the MongoDB holding sites configuration',
        envVariableName: 'APP_MONGO_CONN_URI',
    },
    "mongo-db": {
        description: 'MongoDB database holding sites configuration',
        envVariableName: 'APP_MONGO_DB',
    },
    "apache-sites-dir": {
        defaultValue: '/app/data/apache-sites',
        description: 'The output directory for Apache configuration files',
        envVariableName: 'APP_APACHE_SITES_DIR',
    },
    "php-pool-dir": {
        defaultValue: '/app/data/php-pool',
        description: 'The output directory for PHP configuration files',
        envVariableName: 'APP_PHP_POOL_DIR',
    },
    "php-pool-sockets-dir": {
        defaultValue: '/app/data/php-pool-sockets',
        description: 'The output directory for PHP pool sockets',
        envVariableName: 'APP_PHP_POOL_SOCKETS_DIR',
    },
    "php-pool-sockets-type": {
        defaultValue: 'unix',
        description: 'The type of sockets to use for Apache->PHP communication',
        envVariableName: 'APP_PHP_POOL_SOCKETS_TYPE',
    },
    "mounted-sites-root-dir": {
        defaultValue: '/app/data/sites-root',
        description: 'The mounted (current) root directory of all sites',
        envVariableName: 'APP_MOUNTED_SITES_ROOT_DIR',
    },
    "target-sites-root-dir": {
        defaultValue: '/var/www',
        description: 'The target (production) root directory of all sites',
        envVariableName: 'APP_TARGET_SITES_ROOT_DIR',
    },
    "sftp-keys-dir": {
        defaultValue: '/app/data/sftp-keys/',
        description: 'The output directory holding all SFTP keys per user',
        envVariableName: 'APP_SFTP_KEYS_DIR',
    },
    "apache-gid": {
        defaultValue: 33,
        description: 'The GID of the Apache user',
        envVariableName: 'APP_APACHE_GID',
    },
    "watch": {
        defaultValue: false,
        description: 'The flag indicating to run in watch (infinite) mode',
        envVariableName: 'APP_WATCH',
    },
    "ssl-certs-dir": {
        defaultValue: '/app/data/ssl-certs/',
        description: 'The output directory holding all SSL certificates & keys',
        envVariableName: 'APP_SSL_CERTS_DIR',
    },
    "users-dir": {
        defaultValue: '/app/data/users/',
        description: 'The output directory holding the "passwd" file with all users',
        envVariableName: 'APP_USERS_DIR',
    },
    "k8s-certificate-issuer": {
        defaultValue: '',
        description: 'The K8s certificate issuer in the format "Kind:Name", e.g. "ClusterIssuer:cert-issuer"',
        envVariableName: 'APP_K8S_CERTIFICATE_ISSUER',
    },
    "apache-enabled-ports": {
        defaultValue: 'http, https',
        description: 'Enabled ports in Apache; comma-separated list of values, the options are:'
            + ' http (port 10080), https (port 10443), http-proxy (port 20080), https-proxy (port 20443);'
            + ' the *-proxy protocols have the PROXY protocol enabled',
        envVariableName: 'APP_APACHE_ENABLED_PORTS',
    },
    "apache-default-site": {
        defaultValue: '',
        description: 'Default site (VHost ServerName) for Apache',
        envVariableName: 'APP_APACHE_DEFAULT_SITE',
    },
    "php-version-header-name": {
        description: 'Name of the HTTP header to use to control PHP version used for multi-version PHP sites',
        envVariableName: 'APP_PHP_VERSION_HEADER_NAME',
    },
    "target-namespace": {
        description: 'The K8s namespace where the certificates are to be created',
        envVariableName: 'APP_TARGET_NAMESPACE',
        defaultValue: '',
    },
};

export const argvNames: Array<keyof ArgvType> = Object.keys(argvDescription) as Array<keyof ArgvType>;
export const argvEntries: Array<[keyof ArgvType, ArgvDescriptionItemType<any>]> = Object.entries(argvDescription) as
    Array<[keyof ArgvType, ArgvDescriptionItemType<any>]>;

export const argvSchema: Schema = {
    type: "object",
    properties: Object.fromEntries(
        argvEntries.map(([argName, description]): [keyof ArgvType, Schema] => {
            return [argName, {type: getDefaultValueType(description)}]
        })
    ),
    required: argvNames,
};

function getDefaultValueType(entry: ArgvDescriptionItemType<any>): string {
    return typeof (entry.defaultValue ?? '');
}

export function argvUsage(): string {
    const columns: [string, string][] = Object.entries(argvDescription).map(
            ([argName, argValue]): [string, string] => [
                `  --${argName} <${getDefaultValueType(argValue)}>`,
                `${argValue.description} [or env: ${argValue.envVariableName}]; `
                + (argValue.defaultValue !== undefined
                    ? `defaults to "${argValue.defaultValue}"`
                    : 'no default value')
            ]
        );

    let usage: string = `Usage: ${path.basename(process.argv[1])}\n`;

    if (columns.length > 0) {
        const columnsLengths: number[] = [...columns[0].keys()].map((columnKey: number) =>
            Math.max(...columns.map((row: string[]): number => row[columnKey].length))
        );

        usage += columns
            .map((row: string[]): string[] =>
                row.map((cell: string, index: number): string =>
                    index === 0 ?
                        cell.padEnd(columnsLengths[index], ' ')
                        : cell
                )
            )
            .map((row: string[]): string => row.join(' ')).join('\n');
    }

    return usage;
}
