import {container} from "tsyringe";
import {Settings} from "./Settings";
import {ArgvParser} from "./class/Parser/ArgvParser";
import {ArgvType, argvUsage} from "./schema/argvSchema";
import {KubeConfig} from "@kubernetes/client-node";
import {ArgvUsageException} from "./class/Parser/ArgvUsageException";

container.register<string>('log-level', {useValue: process.env['APP_LOG_LEVEL'] ?? Settings.LOGGING.LOG_LEVEL})
container.register<KubeConfig>(KubeConfig, {useFactory: (): KubeConfig => {
    const kc: KubeConfig = new KubeConfig();

    process.env.KUBECONFIG && process.env.KUBECONFIG.length > 0
        ? kc.loadFromFile(process.env.KUBECONFIG)
        : kc.loadFromCluster();

    return kc;
}})

try {
    container.register<ArgvType>('cli-args', {useValue: container.resolve(ArgvParser).parse()});
} catch (e) {
    if (e instanceof ArgvUsageException) {
        console.log(argvUsage());
        process.exit(2);
    }

    throw e;
}

