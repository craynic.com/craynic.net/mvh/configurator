import {inject, singleton} from "tsyringe";
import {SiteConfig} from "../schema/configSchema";
import {Logger} from "./Logger";
import {
    ChangeStream,
    ChangeStreamDeleteDocument,
    ChangeStreamDocument, ChangeStreamInsertDocument, ChangeStreamReplaceDocument, ChangeStreamUpdateDocument,
    Collection,
    Document,
    FindCursor,
    MongoClient
} from "mongodb";
import {EventEmitter} from "events";
import {ArgvType} from "../schema/argvSchema";

export type MappedSitesConfig = Map<string, SiteConfig>;

export interface SiteDbEvents {
    sites_loaded: (...siteConfigs: SiteConfig[]) => void;
    site_added: (siteConfig: SiteConfig) => void;
    site_updated: (siteConfigBefore: SiteConfig, siteConfigAfter: SiteConfig) => void;
    site_removed: (siteConfig: SiteConfig) => void;
}

@singleton()
export class SitesDb extends EventEmitter {
    private static readonly COLLECTION_NAME: string = 'sites';

    private mongoClient: MongoClient | undefined;
    private mongoCollection: Collection<SiteConfig> | undefined;
    private mongoChangeStream: ChangeStream | undefined;

    private mappedSitesConfig: MappedSitesConfig | undefined;

    // noinspection ES6ClassMemberInitializationOrder
    private _untypedOn = this.on;
    // noinspection ES6ClassMemberInitializationOrder
    private _untypedEmit = this.emit;
    public on = <K extends keyof SiteDbEvents>(event: K, listener: SiteDbEvents[K]): this =>
        this._untypedOn(event, listener)
    public emit = <K extends keyof SiteDbEvents>(event: K, ...args: Parameters<SiteDbEvents[K]>): boolean =>
        this._untypedEmit(event, ...args)

    public constructor(
        private readonly logger: Logger,
        @inject('cli-args') private readonly cliArgs: ArgvType,
    ) {
        super();
    }

    public async setUp(): Promise<SiteConfig[]> {
        this.logger.debug('Connecting to MongoDB...');
        const mongoClient: MongoClient = this.mongoClient = new MongoClient(this.cliArgs['mongo-conn-uri']);

        return mongoClient.connect().then(async (): Promise<SiteConfig[]> => {
            this.logger.info('Connected to MongoDB.');

            this.mongoCollection = mongoClient.db(this.cliArgs['mongo-db']).collection<SiteConfig>(SitesDb.COLLECTION_NAME);

            this.logger.debug('Setting up watches...');
            this.watchForChanges();

            return this.loadSitesConfig()
                .then((mappedSitesConfig: MappedSitesConfig): SiteConfig[] =>
                    [...mappedSitesConfig.values()]
                );
        });
    }

    public async tearDown(): Promise<void> {
        if (this.mongoClient === undefined) {
            return Promise.resolve();
        }

        this.logger.debug('Disconnecting from MongoDB...');

        await this.mongoChangeStream?.close();

        return this.mongoClient.close()
            .then((): void => {
                this.mongoCollection = undefined;
                this.mongoClient = undefined;
                this.mongoChangeStream = undefined;

                this.logger.info('Disconnected from MongoDB.');
            });
    }

    private getMongoCollection(): Collection<SiteConfig> {
        if (this.mongoCollection === undefined) {
            throw 'Connection to MongoDB was not established.';
        }

        return this.mongoCollection;
    }

    private async loadSitesConfig(): Promise<MappedSitesConfig> {
        if (this.mappedSitesConfig !== undefined) {
            return this.mappedSitesConfig;
        }

        const cursor: FindCursor<Document> = this.getMongoCollection()
            .find<Document>({}, {sort: {id: 1}});

        return cursor.toArray()
            .then(
                (sitesConfig: Document[]): MappedSitesConfig =>
                    sitesConfig.reduce<MappedSitesConfig>(
                        (map: MappedSitesConfig, currentValue: Document) =>
                            map.set(String(currentValue._id), currentValue as SiteConfig),
                        new Map<string, SiteConfig>
                    )
            )
            .then((mappedSitesConfig: MappedSitesConfig): MappedSitesConfig => {
                this.mappedSitesConfig = mappedSitesConfig;

                this.logger.info(mappedSitesConfig.size === 1
                    ? `Loaded ${mappedSitesConfig.size} site configurations.`
                    : `Loaded ${mappedSitesConfig.size} sites configurations.`
                );

                this.emit("sites_loaded", ...mappedSitesConfig.values());

                return mappedSitesConfig;
            });
    }

    private watchForChanges(): void {
        if (this.mongoChangeStream !== undefined) {
            throw 'Already watching for changes.';
        }

        this.mongoChangeStream = this.getMongoCollection().watch([], {fullDocument: 'updateLookup'});

        this.mongoChangeStream.on('change', (changedDocument: ChangeStreamDocument): void => {
            switch (changedDocument.operationType) {
                case 'insert':
                    this.onDocumentInsert(changedDocument);
                    break;

                case 'replace':
                case 'update':
                    this.onDocumentUpdate(changedDocument);
                    break;

                case 'delete':
                    this.onDocumentDelete(changedDocument);
                    break;
            }
        });
    }

    private onDocumentInsert(changedDocument: ChangeStreamInsertDocument): void {
        const documentId: string = String(changedDocument.documentKey._id),
            siteConfig: SiteConfig = changedDocument.fullDocument as SiteConfig;

        this.logger.debug(`SiteDB: new site ${siteConfig.name}`);
        this.mappedSitesConfig?.set(documentId, siteConfig);
        this.emit("site_added", siteConfig);
    }

    private onDocumentUpdate(changedDocument: ChangeStreamUpdateDocument | ChangeStreamReplaceDocument): void {
        const documentId: string = String(changedDocument.documentKey._id),
            siteConfigBefore: SiteConfig | undefined = this.mappedSitesConfig?.get(documentId),
            siteConfigAfter: SiteConfig = changedDocument.fullDocument as SiteConfig;

        if (siteConfigBefore === undefined) {
            this.logger.warn(`Unknown site _id ${documentId} updated`);
            return;
        }

        this.logger.debug(`SiteDB: site ${siteConfigBefore.name} updated`);
        this.mappedSitesConfig?.set(documentId, siteConfigAfter);
        this.emit("site_updated", siteConfigBefore, siteConfigAfter);
    }

    private onDocumentDelete(changedDocument: ChangeStreamDeleteDocument): void {
        const documentId: string = String(changedDocument.documentKey._id),
            siteConfigDeleted: SiteConfig | undefined = this.mappedSitesConfig?.get(documentId);

        if (siteConfigDeleted === undefined) {
            this.logger.warn(`Unknown site _id ${changedDocument.documentKey._id} deleted`);
            return;
        }

        this.logger.debug(`SiteDB: site ${siteConfigDeleted.name} deleted`);
        this.mappedSitesConfig?.delete(documentId);
        this.emit("site_removed", siteConfigDeleted);
    }
}
