import {ArgvType} from "../../schema/argvSchema";
import {SiteConfig} from "../../schema/configSchema";
import {inject, singleton} from "tsyringe";
import {Logger} from "../Logger";
import {promiseAllResolved} from "../../function/promiseAllResolved";
import {Settings} from "../../Settings";
import {ConfigProcessor} from "./ConfigProcessor";
import {
    FSDirChangedInformation,
    FSFileContents,
    FSFilename,
    FSFilenameList,
    FSHelper,
    FSPermissions
} from "../Helper/FSHelper";

@singleton()
export class SFTPKeysWriter extends ConfigProcessor {
    public constructor(
        private readonly logger: Logger,
        private readonly fsHelper: FSHelper,
        @inject('cli-args') private readonly cliArgs: ArgvType,
    ) {
        super();
    }

    public async runFull(sitesConfig: SiteConfig[]): Promise<void> {
        this.logger.info('SFTPKeysWriter: full run started...');

        const values: FSDirChangedInformation[] = await promiseAllResolved(
                sitesConfig.map((siteConfig: SiteConfig): Promise<FSDirChangedInformation> =>
                    this.writeSFTPKeysForSite(siteConfig)
                )
            ),
            dirNames: FSFilenameList = new Set(
                values.map(
                    (changedInformation: FSDirChangedInformation): string => {
                        const normalizedDirName: string = changedInformation.absolutePathToDir.endsWith('/')
                            ? changedInformation.absolutePathToDir.slice(0, -1)
                            : changedInformation.absolutePathToDir;

                        return normalizedDirName.substring(normalizedDirName.lastIndexOf('/') + 1);
                    }
                )
            ),
            removedFiles: FSFilenameList =
                await this.fsHelper.emptyDirExceptFiles(this.cliArgs["sftp-keys-dir"], dirNames);

        this.logger.reportRemovedFiles(this.cliArgs["sftp-keys-dir"], ...removedFiles)
        this.logger.info('SFTPKeysWriter: full run finished.');
    }

    public async onSiteAdded(siteConfig: SiteConfig): Promise<void> {
        await this.writeSFTPKeysForSite(siteConfig);
    }

    public async onSiteUpdated(oldSiteConfig: SiteConfig, newSiteConfig: SiteConfig,): Promise<void> {
        await this.onSiteAdded(newSiteConfig);

        if (oldSiteConfig.username !== newSiteConfig.username) {
            await this.onSiteRemoved(oldSiteConfig);
        }
    }

    public async onSiteRemoved(siteConfig: SiteConfig): Promise<void> {
        const removedDirs: FSFilenameList =
            await this.fsHelper.remove(this.cliArgs["sftp-keys-dir"], siteConfig.username);

        this.logger.reportRemovedFiles(this.cliArgs["sftp-keys-dir"], ...removedDirs);
    }

    private async writeSFTPKeysForSite(siteConfig: SiteConfig): Promise<FSDirChangedInformation> {
        const filesToWrite: Map<FSFilename, FSFileContents> = new Map<FSFilename, FSFileContents>();

        if (siteConfig.sftp !== undefined && siteConfig.sftp.keys !== undefined) {
            filesToWrite.set('authorized_keys', siteConfig.sftp.keys.join("\n"));
        }

        await this.fsHelper.ensureDir(
            this.cliArgs["sftp-keys-dir"],
            {uid: 0, gid: 0, mode: Settings.SFTP_KEYS_PERMISSIONS.ROOT_DIR} as FSPermissions
        );

        const dir: FSFilename = `${this.cliArgs["sftp-keys-dir"]}/${siteConfig.username}`;

        await this.fsHelper.ensureDir(
            dir,
            {uid: siteConfig.id, gid: siteConfig.id, mode: Settings.SFTP_KEYS_PERMISSIONS.DIRS} as FSPermissions
        );

        const changedInfo: FSDirChangedInformation = await this.fsHelper.syncFilesToDir(
            dir,
            filesToWrite,
            {uid: siteConfig.id, gid: siteConfig.id, mode: Settings.SFTP_KEYS_PERMISSIONS.FILES} as FSPermissions
        );

        this.logger.reportDirChangedInformation(changedInfo);

        return changedInfo;
    }
}
