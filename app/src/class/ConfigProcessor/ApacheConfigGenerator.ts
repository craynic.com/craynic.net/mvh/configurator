import {inject, singleton} from "tsyringe";
import {PhpVersion, SiteConfig} from "../../schema/configSchema";
import {ArgvType} from "../../schema/argvSchema";
import {Logger} from "../Logger";
import {Settings} from "../../Settings";
import {ConfigProcessor} from "./ConfigProcessor";
import {
    FSFileContents,
    FSFileList,
    FSFilename,
    FSFilenameList,
    FSHelper
} from "../Helper/FSHelper";
import {Dir, HomeDirHelper, PathType} from "../Helper/HomeDirHelper";
import {PhpHelper} from "../Helper/PhpHelper";
import {
    K8sCertificateManager,
    TLSCertificate,
    TLSCertificateForSite,
    TLSCertificatesForSites
} from "../K8sCertificateManager";
import {CancellablePromise} from "real-cancellable-promise";

enum ApachePortName {
    "http",
    "https",
    "http-proxy",
    "https-proxy",
}

const apachePortValues: { [key in keyof typeof ApachePortName]: number; } = {
    "http": 10080,
    "https": 10443,
    "http-proxy": 20080,
    "https-proxy": 20443,
}

const apachePortProxySettings: { [key in keyof typeof ApachePortName]: boolean; } = {
    "http": false,
    "https": false,
    "http-proxy": true,
    "https-proxy": true,
}

const apachePortSSLSettings: { [key in keyof typeof ApachePortName]: boolean; } = {
    "http": false,
    "https": true,
    "http-proxy": false,
    "https-proxy": true,
}

@singleton()
export class ApacheConfigGenerator extends ConfigProcessor {
    private readonly apacheEnabledPorts: Array<ApachePortName> = [];

    public constructor(
        private readonly logger: Logger,
        private readonly fsHelper: FSHelper,
        private readonly homeDirHelper: HomeDirHelper,
        private readonly phpHelper: PhpHelper,
        private readonly k8sCertificateManager: K8sCertificateManager,
        @inject('cli-args') private readonly cliArgs: ArgvType,
    ) {
        super();

        this.apacheEnabledPorts = this.getApacheEnabledPorts();
    }

    private getApacheEnabledPorts(): ApachePortName[] {
        return this.cliArgs["apache-enabled-ports"]
            .split(',')
            .map((value: string): string => value.trim())
            .filter((value: string): boolean => value.length > 0)
            .map((value: string): ApachePortName => {
                if (this.isApachePortName(value)) {
                    return ApachePortName[value];
                } else {
                    throw new Error(`Invalid Apache port name: ${value}`);
                }
            });
    }

    private isApachePortName(value: string): value is keyof typeof ApachePortName {
        return Object.keys(ApachePortName).includes(value);
    }

    public async runFull(sitesConfig: SiteConfig[]): Promise<void> {
        this.logger.info('ApacheConfigGenerator: full run started...');

        await this.fsHelper.ensureDir(this.cliArgs["ssl-certs-dir"], Settings.CONFIG_DIRS_PERMISSIONS);
        await this.fsHelper.ensureDir(this.cliArgs["apache-sites-dir"], Settings.CONFIG_DIRS_PERMISSIONS);

        const tlsCertificatesForSites: TLSCertificatesForSites =
            await this.k8sCertificateManager.getCertificatesForAllSites(sitesConfig);

        this.logger.reportDirChangedInformation(
            await this.fsHelper.syncFilesToDir(
                this.cliArgs["apache-sites-dir"],
                this.generateFilesForAllSites(sitesConfig, tlsCertificatesForSites),
                Settings.CONFIG_FILES_PERMISSIONS
            )
        );

        this.logger.reportDirChangedInformation(
            await this.fsHelper.syncFilesToDir(
                this.cliArgs["apache-sites-dir"],
                new Map<FSFilename, FSFileContents>([[Settings.READY_FILE_FILENAME, '']]),
                Settings.CONFIG_FILES_PERMISSIONS,
                false
            )
        );

        this.logger.info('ApacheConfigGenerator: full run finished.');
    }

    public async onSiteAdded(siteConfig: SiteConfig): Promise<void> {
        const tlsCertificatesForSite: TLSCertificateForSite[] =
            await this.k8sCertificateManager.getCertificatesForSite(siteConfig);

        this.logger.reportDirChangedInformation(
            await this.fsHelper.syncFilesToDir(
                this.cliArgs["apache-sites-dir"],
                this.generateFilesForSite(siteConfig, tlsCertificatesForSite),
                Settings.CONFIG_FILES_PERMISSIONS,
                false
            )
        );
    }

    public async onSiteUpdated(oldSiteConfig: SiteConfig, newSiteConfig: SiteConfig): Promise<void> {
        await this.onSiteAdded(newSiteConfig);

        if (this.getSiteConfigFileName(oldSiteConfig) !== this.getSiteConfigFileName(newSiteConfig)) {
            await this.onSiteRemoved(oldSiteConfig);
        }
    }

    public async onSiteRemoved(siteConfig: SiteConfig): Promise<void> {
        const removedFileNames: FSFilenameList =
            await this.fsHelper.remove(this.cliArgs["apache-sites-dir"], this.getSiteConfigFileName(siteConfig));

        this.logger.reportRemovedFiles(this.cliArgs["apache-sites-dir"], ...removedFileNames);

        this.logger.info(
            `Waiting ${Settings.GRACE_TIME_FOR_SERVERS_RELOAD} secs for Apache to pick up changes...`
        );

        await CancellablePromise.delay(1000 * Settings.GRACE_TIME_FOR_SERVERS_RELOAD);
        await this.k8sCertificateManager.onSiteRemoved(siteConfig);
    }

    private generateFilesForAllSites(siteConfigs: SiteConfig[], tlsCertificates: TLSCertificatesForSites): FSFileList {
        return new Map<FSFilename, FSFileContents>(
            [...siteConfigs
                .map((siteConfig: SiteConfig): [FSFilename, FSFileContents] => {
                    if (!tlsCertificates.has(siteConfig.id)) {
                        throw new Error(`Cannot generate config files for all sites as site ${siteConfig.name}`
                            + ` does not have any TLS certificates generated.`
                        );
                    }

                    return [
                        this.getSiteConfigFileName(siteConfig),
                        this.generateSiteConfig(siteConfig, tlsCertificates.get(siteConfig.id)!),
                    ];
                }),
                [this.getListenConfigFileName(), this.generateListenConfigFile()]
            ]
        );
    }

    private generateListenConfigFile(): string {
        return this.apacheEnabledPorts
            .map((port: ApachePortName): string => {
                const portNumber: number = apachePortValues[ApachePortName[port] as keyof typeof ApachePortName],
                    portIsHTTPS: boolean = apachePortSSLSettings[ApachePortName[port] as keyof typeof ApachePortName];

                return `Listen ${portNumber} ${portIsHTTPS ? 'https' : 'http'}`;
            })
            .join("\n");
    }

    private generateFilesForSite(siteConfig: SiteConfig, tlsCertificates: TLSCertificateForSite[]): FSFileList {
        return new Map<FSFilename, FSFileContents>([[
            this.getSiteConfigFileName(siteConfig),
            this.generateSiteConfig(siteConfig, tlsCertificates),
        ]]);
    }

    private getListenConfigFileName(): FSFilename {
        return `000-listen.conf`;
    }

    private getSiteConfigFileName(siteConfig: SiteConfig): FSFilename {
        return this.isDefaultSite(siteConfig)
            ? `001-${siteConfig.name}.conf`
            : `002-${siteConfig.name}.conf`;
    }

    private isDefaultSite(siteConfig: SiteConfig): boolean {
        return (siteConfig.aliases ?? []).reduce<boolean>(
            (previousValue: boolean, currentValue: string): boolean =>
                previousValue || (this.cliArgs["apache-default-site"] === currentValue),
            this.cliArgs["apache-default-site"] === siteConfig.name
        );
    }

    private generateSiteConfig(siteConfig: SiteConfig, tlsCertificates: TLSCertificateForSite[]): FSFileContents {
        return tlsCertificates
            .map((tlsCertificate: TLSCertificateForSite): FSFileContents => {
                if (tlsCertificate.hostNames.length === 0) {
                    throw new Error(`Cannot generate config file for site ${siteConfig.name}`
                        + ` as the certificate hostnames list is empty.`
                    );
                }

                const serverName: string = tlsCertificate.hostNames[0],
                    serverAliases: string[] = tlsCertificate.hostNames.slice(1);

                return this.generatePartialSiteConfig(
                    siteConfig,
                    serverName,
                    serverAliases,
                    tlsCertificate.tlsCertificate
                );
            })
            .join("\n\n");
    }

    private generatePartialSiteConfig(
        config: SiteConfig,
        serverName: string,
        serverAliases: string[],
        tlsCertificate: TLSCertificate
    ): FSFileContents {
        const publicKeyFileName: string = `/etc/ssl/apache2/${tlsCertificate.publicKeyFilename}`,
            privateKeyFileName: string = `/etc/ssl/apache2/${tlsCertificate.privateKeyFilename}`,
            sslConfig: string = `SSLEngine on
SSLCertificateFile ${publicKeyFileName}
SSLCertificateKeyFile ${privateKeyFileName}`,
            proxyConfig: string = 'RemoteIPProxyProtocol on';

        return this.apacheEnabledPorts
            .map((port: ApachePortName): string => {
                const portNumber: number =
                        apachePortValues[ApachePortName[port] as keyof typeof ApachePortName],
                    proxyEnabled: boolean =
                        apachePortProxySettings[ApachePortName[port] as keyof typeof ApachePortName],
                    sslEnabled: boolean =
                        apachePortSSLSettings[ApachePortName[port] as keyof typeof ApachePortName],
                    generatedConfig: string = this.generateVirtualHostConfig(
                        config,
                        serverName,
                        serverAliases,
                        portNumber,
                        [proxyEnabled ? proxyConfig : '', sslEnabled ? sslConfig : ''].join("\n\n").trim()
                    );

                return apachePortSSLSettings[ApachePortName[port] as keyof typeof ApachePortName]
                    ? this.wrapVirtualHostConfigWithSSL(publicKeyFileName, privateKeyFileName, generatedConfig)
                    : generatedConfig;
            })
            .join("\n\n");
    }

    private wrapVirtualHostConfigWithSSL(
        publicKeyFileName: string,
        privateKeyFileName: string,
        virtualHostConfig: string,
    ): string {
        return `<IfModule mod_ssl.c>
<IfFile ${publicKeyFileName}>
<IfFile ${privateKeyFileName}>
${virtualHostConfig}
</IfFile>
</IfFile>
</IfModule>`;
    }

    private generateVirtualHostConfig(
        config: SiteConfig,
        serverName: string,
        serverAliases: string[],
        port: number,
        additionalConfig: string,
    ): FSFileContents {
        const targetSitesRootDirHtdocs: FSFilename =
                this.homeDirHelper.getPath(config, PathType.TARGET, Dir.HTDOCS),
            targetSitesRootDirLogsApache: FSFilename =
                this.homeDirHelper.getPath(config, PathType.TARGET, Dir.LOGS_APACHE);

        return `<VirtualHost *:${port}>
    ServerName ${serverName}
    ${
            (serverAliases.length > 0) ? `ServerAlias ${serverAliases.join(' ')}\n` : ''
        }
    CustomLog ${targetSitesRootDirLogsApache}/access.log vhost_combined
    ErrorLog ${targetSitesRootDirLogsApache}/error.log

    DocumentRoot "${targetSitesRootDirHtdocs}"

    <Directory "${targetSitesRootDirHtdocs}">
        AllowOverride All
    </Directory>

${
            this.generateVirtualHostConfigAddHandler(config)
                .split("\n")
                .map((line: string): string => `    ${line}`)
                .join("\n")
    }
${
            additionalConfig !== ''
                ? "\n" + additionalConfig.split("\n").map((line: string): string => `    ${line}`).join("\n") + "\n"
                : ''
        }</VirtualHost>`;
    }

    private generateVirtualHostConfigAddHandler(config: SiteConfig): FSFileContents {
        const defaultAddHandler: FSFileContents = this.getAddhandlerDirectiveForSite(config, config.php.version);

        if (!this.phpHelper.hasSiteAllPhpVersionsConfigured(config)) {
            return defaultAddHandler;
        }

        const phpVersionToHandlerMap: Map<PhpVersion, FSFileContents> = new Map<PhpVersion, FSFileContents>(
            [...this.phpHelper.getPhpVersionsToConfigureForSite(config)]
                .map((phpVersion: PhpVersion): [PhpVersion, FSFileContents] => [
                    phpVersion,
                    this.getAddhandlerDirectiveForSite(config, phpVersion)
                ])
        );

        return [...phpVersionToHandlerMap]
            .reduce<FSFileContents>(
                (previousValue: FSFileContents, currentValue: [PhpVersion, FSFileContents]): FSFileContents => {
                    return `${previousValue}
                    
<If "%{HTTP:${this.cliArgs["php-version-header-name"]}} = '${currentValue[0]}'">
    ${currentValue[1]}
</If>`;
                },
                defaultAddHandler
            );
    }

    private getAddhandlerDirectiveForSite(siteConfig: SiteConfig, phpVersion: PhpVersion): FSFileContents {
        const phpFileExtensions: string[] = ['php', 'phps', 'phar'],
            phpFileExtensionsRendered: string = phpFileExtensions
                .map((extension: string): string => `.${extension}`)
                .join(' ');

        return this.phpHelper.useUnixSockets()
            ? `AddHandler "proxy:unix:${
                    this.phpHelper.getPoolSocketPath(siteConfig, phpVersion)
                }|fcgi://localhost" ${phpFileExtensionsRendered}`
            : `AddHandler "proxy:fcgi://${
                    this.phpHelper.getPoolHostPort(siteConfig, phpVersion)
                }" ${phpFileExtensionsRendered}`;
    }
}
