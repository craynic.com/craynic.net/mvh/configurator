import {KubeConfig} from "@kubernetes/client-node";
import * as k8s from "@kubernetes/client-node";
import {Logger} from "./Logger";
import {EventEmitter} from "events";

interface Events {
    added: (apiObj: any) => void;
    modified: (apiObj: any) => void;
    deleted: (apiObj: any) => void;
}

enum RequestState {
    STOPPED,
    STARTED,
    ABORTED,
}
type AbortPromiseResolve = () => void;

declare type ConnResetError = Error & {
    code: 'ECONNRESET'
}

export class InfiniteK8sWatch extends EventEmitter {
    private abortController: AbortController | null = null;
    private requestState: RequestState = RequestState.STOPPED;
    private lastResourceVersion: string | null = null;
    private abortPromiseResolveCollection: AbortPromiseResolve[] = [];

    // noinspection ES6ClassMemberInitializationOrder
    private _untypedOn = this.on;
    // noinspection ES6ClassMemberInitializationOrder
    private _untypedEmit = this.emit;
    public on = <K extends keyof Events>(event: K, listener: Events[K]): this =>
        this._untypedOn(event, listener)
    public emit = <K extends keyof Events>(event: K, ...args: Parameters<Events[K]>): boolean =>
        this._untypedEmit(event, ...args)

    public constructor(
        private readonly kc: KubeConfig,
        private readonly logger: Logger,
    ) {
        super();
    }

    public async start(path: string, resourceVersion: string, queryParams: object = {}): Promise<void> {
        if (this.requestState !== RequestState.STOPPED) {
            throw new Error('Watch is already started.');
        }

        this.requestState = RequestState.STARTED;
        this.lastResourceVersion = resourceVersion;

        const watch = new k8s.Watch(this.kc);

        return watch.watch(
            path,
            {
                allowWatchBookmarks: true,
                resourceVersion,
                ...queryParams
            },
            (phase: string, apiObj: any): void => {
                if (phase === 'ADDED') {
                    this.lastResourceVersion = apiObj.metadata.resourceVersion;
                    this.emit('added', apiObj);
                } else if (phase === 'MODIFIED') {
                    this.lastResourceVersion = apiObj.metadata.resourceVersion;
                    this.emit('modified', apiObj);
                } else if (phase === 'DELETED') {
                    this.lastResourceVersion = apiObj.metadata.resourceVersion;
                    this.emit('deleted', apiObj);
                } else if (phase === 'BOOKMARK') {
                    this.lastResourceVersion = apiObj.metadata.resourceVersion;
                    this.logger.debug(`Watch for path ${path} bookmarked at ${this.lastResourceVersion}`);
                }
            },
            (err: any): void =>
                this.requestState === RequestState.ABORTED
                    ? this.onAbort(path)
                    : this.onDone(path, queryParams, err)
        )
        .then((abortController: AbortController): void => {
            this.abortController = abortController;

            if (this.requestState === RequestState.ABORTED) {
                this.abortController.abort();
            }
        });
    }

    public async abort(): Promise<void> {
        if (this.requestState === RequestState.STOPPED) {
            throw new Error('Watch is stopped.');
        }

        this.requestState = RequestState.ABORTED;

        const abortPromise: Promise<void> = new Promise<void>((resolve: AbortPromiseResolve): void => {
            this.abortPromiseResolveCollection.push(resolve);
        });

        // request result might not be set yet, even though we are already in state "STARTED" or "ABORTED";
        // that's only possible if we cancel to quickly after start before the promise creates the request
        // in that case, do nothing here and let the promise abort the result after creation
        if (this.abortController !== null) {
            this.abortController.abort();
        }

        return abortPromise;
    }

    private onAbort(path: string): void {
        this.logger.debug(`Watch request for ${path} aborted.`);
        this.reset();

        const resolveFunctions: AbortPromiseResolve[] = this.abortPromiseResolveCollection;
        this.abortPromiseResolveCollection = [];

        resolveFunctions.forEach((resolveFunction: AbortPromiseResolve): void => {
            resolveFunction();
        })
    }

    private isConnResetError(err: any): err is ConnResetError {
        return typeof err === 'object'
            && err.hasOwnProperty('code')
            && err.code === 'ECONNRESET';
    }

    private onDone(path: string, queryParams: object, error: object|string|null): void {
        this.reset();

        // error came, throw it
        this.checkDoneError(path, error);

        this.start(path, this.lastResourceVersion!, queryParams)
            .then((): void => {});
    }

    private checkDoneError(path: string, error: object|string|null): void {
        // no error = request finished, restart it
        if (error === null) {
            this.logger.debug(
                `Watch for path ${path} timeouted, restarting from resourceVersion ${this.lastResourceVersion!}...`
            );
            return;
        }

        // connection was reset, restart it
        if (this.isConnResetError(error)) {
            this.logger.debug(`Ignoring K8s connection reset error "${error.message}"`
                + ` and restarting from resourceVersion ${this.lastResourceVersion!}...`);
            return;
        }

        // unknown error came, throw it
        throw error;
    }

    private reset(): void {
        this.requestState = RequestState.STOPPED;

        this.abortController = null;
    }
}
