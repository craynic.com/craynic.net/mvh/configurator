import {ValidationError, Validator} from "jsonschema";
import {singleton} from "tsyringe";
import {
    argvDescription,
    argvNames,
    ArgvPartialType,
    argvSchema,
    ArgvType
} from "../../schema/argvSchema";
import {Logger} from "../Logger";
import minimist from "minimist";
import {ArgvUsageException} from "./ArgvUsageException";

@singleton()
export class ArgvParser {
    public constructor(
        private readonly jsonValidator: Validator,
        private readonly logger: Logger
    ) {
    }

    public parse(): ArgvType {
        // parse CLI arguments & validate them against schema
        const parsedArgs: ArgvType = this.composePartialParses(
            this.parseCli(),
            this.parseEnv(),
            this.getArgvDefaults(),
        );

        this.logger.debug('Parsed settings: ' + JSON.stringify(parsedArgs));

        try {
            this.jsonValidator.validate(parsedArgs, argvSchema, {throwError: true});
        } catch (error: unknown) {
            if (error instanceof ValidationError) {
                this.logger.debug(error.stack);
                throw new ArgvUsageException();
            } else {
                throw error;
            }
        }

        return parsedArgs;
    }

    private parseCli(): ArgvPartialType {
        const argNames: string[] = Object.keys(argvDescription),
            parsedArgs: {[name: string]: string} = minimist<{[name: string]: string}>(
                process.argv.slice(2),
                {string: argNames}
            );

        return Object.fromEntries(
            Object
                .entries(parsedArgs)
                .filter(([name, _]: [string, string]): boolean => argNames.includes(name))
                .map(([name, value]: [string, string]): [string, string|number|boolean] => {
                    return [name, this.parseCliValue(name, value)];
                })
        );
    }

    private parseEnv(): ArgvPartialType {
        return Object.fromEntries(
            argvNames.map((argvName: keyof ArgvType): [keyof ArgvType, any] => {
                return [
                    argvName,
                    this.parseEnvValue(argvName, process.env[argvDescription[argvName].envVariableName] ?? null)
                ];
            })
        ) as ArgvPartialType;
    }

    private parseEnvValue(argvName: string, value: string|null|undefined): string|number|null|boolean {
        return (value == null || value == "")
            ? null
            : this.parseCliValue(argvName, value);
    }

    private parseCliValue(argvName: string, value: string): string|number|boolean {
        if (typeof argvDescription[argvName].defaultValue === "number") {
            return parseInt(value);
        }

        if (typeof argvDescription[argvName].defaultValue === "boolean") {
            return ["true", "yes", "1", ""].includes(value.toLowerCase());
        }

        // string
        return value;
    }

    private getArgvDefaults(): ArgvType {
        return Object.fromEntries(
            argvNames.map((argvName: keyof ArgvType): [keyof ArgvType, any] => {
                return [argvName, argvDescription[argvName].defaultValue];
            })
        ) as ArgvType;
    }

    private composePartialParses(...parses: ArgvPartialType[]): ArgvType {
        return Object.fromEntries(
            argvNames.map((argName: keyof ArgvType): [keyof ArgvType, any] => {
                return [
                    argName,
                    parses
                        .map((parse: ArgvPartialType): any => parse[argName])
                        .find((value: any): boolean => value != null)
                ];
            })) as ArgvType;
    }
}
