import {inject, singleton} from "tsyringe";
import {SitesDb} from "./SitesDb";
import {Logger} from "./Logger";
import {ArgvType} from "../schema/argvSchema";
import {SiteConfig} from "../schema/configSchema";
import {RunOrchestrator} from "./RunOrchestrator";
import {K8sCertificateManager} from "./K8sCertificateManager";

@singleton()
export class Main {
    public constructor(
        private readonly logger: Logger,
        private readonly sitesDb: SitesDb,
        private readonly runOrchestrator: RunOrchestrator,
        private readonly k8sCertificateManager: K8sCertificateManager,
        @inject('cli-args') private readonly cliArgs: ArgvType,
    ) {
    }

    public async run(): Promise<void> {
        this.logger.info("Process starting");

        process.on('SIGINT', (): void => {
            this.shutDown();
        });

        return this.k8sCertificateManager.setUp()
            .then(async (): Promise<SiteConfig[]> =>
                this.sitesDb.setUp())
            .then(async (siteConfigs: SiteConfig[]): Promise<void> =>
                this.runOrchestrator.runFull(siteConfigs))
            .then(async (): Promise<void> => {
                if (!this.cliArgs.watch) {
                    return this.shutDown();
                }

                this.logger.info('Entering watch mode');

                return this.runOrchestrator.processPendingChanges();
            });
    }

    private async shutDown(): Promise<void> {
        return this.sitesDb.tearDown()
            .then(async (): Promise<void> => this.k8sCertificateManager.tearDown())
            .then((): void => this.logger.info('Process finished'));
    }
}
