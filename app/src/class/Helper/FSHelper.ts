import {singleton} from "tsyringe";
import fs from "fs-extra";
import {promiseAllResolved} from "../../function/promiseAllResolved";

export type FSPermissions = {
    uid: null,
    gid: null,
    mode: number,
} | {
    uid: number,
    gid: number,
    mode: number,
};

export type FSFilename = string;
export type FSFilenameList = Set<FSFilename>;
export type FSFileContents = string;
export type FSFileStruct = { filename: FSFilename, contents: FSFileContents};
export type FSFileList = Map<FSFilename, FSFileContents>;

export enum FSFileWriteStatus {
    Created = 1,
    Changed,
    Unchanged,
}

export class FSDirChangedInformation {
    constructor(
        public readonly absolutePathToDir: string,
        public readonly createdFiles: FSFilenameList,
        public readonly changedFiles: FSFilenameList,
        public readonly unchangedFiles: FSFilenameList,
        public readonly removedFiles: FSFilenameList,
    ) {
    }
}

@singleton()
export class FSHelper {
    public async ensureDir(absolutePath: string, permissions: FSPermissions): Promise<void> {
        if (this.existsPathSync(absolutePath)) {
            return;
        }

        return fs.ensureDir(absolutePath, {mode: permissions.mode})
            .then(
                (): Promise<void> => FSHelper.setPermissions(absolutePath, permissions)
            );
    }

    public ensureDirSync(absolutePath: string, permissions: FSPermissions): void {
        if (this.existsPathSync(absolutePath)) {
            return;
        }

        fs.ensureDirSync(absolutePath, {mode: permissions.mode});
        FSHelper.setPermissionsSync(absolutePath, permissions);
    }

    public async remove(absolutePathToDir: FSFilename, ...fileNames: FSFilename[]): Promise<FSFilenameList> {
        const existingFileNames: FSFilename[] = this.filterExisting(absolutePathToDir, ...fileNames);

        return promiseAllResolved<void>(
            existingFileNames.map(
                async (existingFileName: FSFilename): Promise<void> =>
                    fs.remove(`${absolutePathToDir}/${existingFileName}`)
            )
        )
        .then((): FSFilenameList => new Set<FSFilename>(existingFileNames));
    }

    public removeSync(absolutePathToDir: FSFilename, ...fileNames: FSFilename[]): FSFilenameList {
        const existingFileNames: FSFilename[] = this.filterExisting(absolutePathToDir, ...fileNames);

        existingFileNames.forEach(
            (existingFileName: FSFilename): void =>
                fs.removeSync(`${absolutePathToDir}/${existingFileName}`)
        );

        return new Set<FSFilename>(existingFileNames);
    }

    private filterExisting(absolutePathToDir: FSFilename, ...fileNames: FSFilename[]): FSFilename[] {
        return fileNames.filter(
            (fileName: FSFilename): boolean => fs.existsSync(`${absolutePathToDir}/${fileName}`)
        );
    }

    public async rename(oldPath: string, newPath: string): Promise<void> {
        return fs.rename(oldPath, newPath);
    }

    public async emptyDirExceptFiles(absolutePath: string, filesToKeep: FSFilenameList): Promise<FSFilenameList> {
        const removedFiles: FSFilenameList = new Set();

        return fs.readdir(absolutePath)
            .then((filesInDir: string[]): Promise<void[]> => promiseAllResolved(
                filesInDir
                    .filter((fileInDir: string): boolean => !filesToKeep.has(fileInDir))
                    .map((fileToRemove: string): Promise<void> => {
                        removedFiles.add(fileToRemove);
                        return fs.remove(`${absolutePath}/${fileToRemove}`);
                    })
            ))
            .then((): FSFilenameList => removedFiles);
    }

    public emptyDirExceptFilesSync(absolutePath: string, filesToKeep: FSFilenameList): FSFilenameList {
        const removedFiles: FSFilenameList = new Set(),
            filesInDir: string[] = fs.readdirSync(absolutePath);

        filesInDir
            .filter((fileInDir: string): boolean => !filesToKeep.has(fileInDir))
            .forEach((fileToRemove: string): void => {
                removedFiles.add(fileToRemove);
                fs.removeSync(`${absolutePath}/${fileToRemove}`);
            });

        return removedFiles;
    }

    public async writeFile(absolutePath: string, data: string, permissions: FSPermissions): Promise<void> {
        return fs.writeFile(absolutePath, data).then(
            (): Promise<void> => FSHelper.setPermissions(absolutePath, permissions)
        );
    }

    public writeFileSync(absolutePath: string, data: string, permissions: FSPermissions): void {
        fs.writeFileSync(absolutePath, data);

        FSHelper.setPermissionsSync(absolutePath, permissions);
    }

    public async writeFileIfChanged(
        absolutePathToDir: FSFilename,
        fileName: FSFilename,
        data: string,
        permissions: FSPermissions
    ): Promise<FSFileWriteStatus> {
        const absolutePath: FSFilename = `${absolutePathToDir}/${fileName}`;

        if (!this.existsPathSync(absolutePath)) {
            return this.writeFile(absolutePath, data, permissions)
                .then((): FSFileWriteStatus => FSFileWriteStatus.Created);
        }

        return fs.readFile(absolutePath)
            .then(async (fileContents: Buffer): Promise<FSFileWriteStatus> => {
                if (fileContents.toString() === data) {
                    return Promise.resolve(FSFileWriteStatus.Unchanged);
                }

                return this.writeFile(absolutePath, data, permissions)
                    .then((): FSFileWriteStatus => FSFileWriteStatus.Changed);
            });
    }

    public writeFileIfChangedSync(
        absolutePathToDir: string,
        fileName: FSFilename,
        data: string,
        permissions: FSPermissions
    ): FSFileWriteStatus {
        const absolutePath: FSFilename = `${absolutePathToDir}/${fileName}`;

        if (!this.existsPathSync(absolutePath)) {
            this.writeFileSync(absolutePath, data, permissions);

            return FSFileWriteStatus.Created;
        }

        const fileContents: Buffer = fs.readFileSync(absolutePath);

        if (fileContents.toString() === data) {
            return FSFileWriteStatus.Unchanged;
        }

        this.writeFileSync(absolutePath, data, permissions);

        return FSFileWriteStatus.Changed;
    }

    private static async setPermissions(absolutePath: string, permissions: FSPermissions): Promise<void> {
        // noinspection JSVoidFunctionReturnValueUsed
        return fs.chmod(absolutePath, permissions.mode)
            .then(async (): Promise<void> => {
                if (permissions.uid !== null) {
                    return fs.chown(absolutePath, permissions.uid, permissions.gid);
                }
            });
    }

    private static setPermissionsSync(absolutePath: string, permissions: FSPermissions): void {
        fs.chmodSync(absolutePath, permissions.mode);

        if (permissions.uid !== null) {
            fs.chownSync(absolutePath, permissions.uid, permissions.gid);
        }
    }

    private existsPathSync(path: string): boolean {
        return fs.existsSync(path);
    }

    public async syncFilesToDir(
        absolutePathToDir: string,
        files: FSFileList,
        filesPermissions: FSPermissions,
        shouldRemoveRedundantFiles: boolean = true,
    ): Promise<FSDirChangedInformation> {
        const createdFiles: FSFilenameList = new Set<FSFilename>(),
            changedFiles: FSFilenameList = new Set<FSFilename>(),
            unchangedFiles: FSFilenameList = new Set<FSFilename>();

        const writePromise: Promise<void[]> = promiseAllResolved(
            [...files.entries()]
                .map(async ([fileName, fileContents]: [FSFilename, FSFileContents]): Promise<void> => {
                    return this
                        .writeFileIfChanged(
                            absolutePathToDir,
                            fileName,
                            fileContents,
                            filesPermissions
                        )
                        .then((fileWriteStatus: FSFileWriteStatus): void => {
                            if (fileWriteStatus === FSFileWriteStatus.Created) {
                                createdFiles.add(fileName);
                            } else if (fileWriteStatus === FSFileWriteStatus.Changed) {
                                changedFiles.add(fileName);
                            } else {
                                unchangedFiles.add(fileName);
                            }
                        });
                })
        );

        const deletePromise: Promise<FSFilenameList> = shouldRemoveRedundantFiles
            ? this.emptyDirExceptFiles(absolutePathToDir, new Set<string>(files.keys()))
            : Promise.resolve<FSFilenameList>(new Set<FSFilename>());

        return writePromise
            .then((): Promise<FSFilenameList> => deletePromise)
            .then((deletedFiles: FSFilenameList): FSDirChangedInformation => new FSDirChangedInformation(
                absolutePathToDir, createdFiles, changedFiles, unchangedFiles, deletedFiles
            ));
    }

    public syncFilesToDirSync(
        absolutePathToDir: string,
        files: FSFileList,
        filesPermissions: FSPermissions,
        shouldRemoveRedundantFiles: boolean = true,
    ): FSDirChangedInformation {
        const createdFiles: FSFilenameList = new Set<FSFilename>(),
            changedFiles: FSFilenameList = new Set<FSFilename>(),
            unchangedFiles: FSFilenameList = new Set<FSFilename>();

        files.forEach((fileContents: FSFileContents, fileName: FSFilename): void => {
            const fileWriteStatus: FSFileWriteStatus = this.writeFileIfChangedSync(
                absolutePathToDir,
                fileName,
                fileContents,
                filesPermissions
            );

            if (fileWriteStatus === FSFileWriteStatus.Created) {
                createdFiles.add(fileName);
            } else if (fileWriteStatus === FSFileWriteStatus.Changed) {
                changedFiles.add(fileName);
            } else {
                unchangedFiles.add(fileName);
            }
        });

        const deletedFiles: FSFilenameList = shouldRemoveRedundantFiles
            ? this.emptyDirExceptFilesSync(absolutePathToDir, new Set<string>(files.keys()))
            : new Set<FSFilename>();

        return new FSDirChangedInformation(
            absolutePathToDir, createdFiles, changedFiles, unchangedFiles, deletedFiles
        );
    }
}
