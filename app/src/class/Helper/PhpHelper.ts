import {inject, singleton} from "tsyringe";
import {ArgvType} from "../../schema/argvSchema";
import {PhpVersion, PhpVersions, SiteConfig} from "../../schema/configSchema";
import {FSFilename} from "./FSHelper";

@singleton()
export class PhpHelper {
    public constructor(
        @inject('cli-args') private readonly cliArgs: ArgvType,
    ) {
    }

    public useUnixSockets(): boolean {
        return this.cliArgs['php-pool-sockets-type'] === 'unix';
    }

    public getPoolPortNumber(siteConfig: SiteConfig): number {
        return siteConfig.id;
    }

    public getPoolHostPort(siteConfig: SiteConfig, phpVersion: PhpVersion): string {
        return `php${phpVersion.replaceAll('.', '')}:${this.getPoolPortNumber(siteConfig)}`;
    }

    public getPoolSocketRootPathForPhpVersion(phpVersion: PhpVersion): FSFilename {
        return `${this.cliArgs["php-pool-sockets-dir"]}/${phpVersion}`;
    }

    public getPoolSocketPath(siteConfig: SiteConfig, phpVersion: PhpVersion): FSFilename {
        return `${this.getPoolSocketRootPathForPhpVersion(phpVersion)}/${siteConfig.name}`;
    }

    public hasSiteAllPhpVersionsConfigured(siteConfig: SiteConfig): boolean {
        return this.cliArgs["php-version-header-name"] !== ''
            && siteConfig.php.enable_all_versions === true;
    }

    public getPhpVersionsToConfigureForSite(siteConfig: SiteConfig): Set<PhpVersion> {
        return new Set<PhpVersion>(
            this.hasSiteAllPhpVersionsConfigured(siteConfig)
                ? PhpVersions
                : [siteConfig.php.version]
        );
    }

    public havePhpVersionsChanged(oldSiteConfig: SiteConfig, newSiteConfig: SiteConfig): boolean {
        const oldPhpVersions: Set<PhpVersion> = this.getPhpVersionsToConfigureForSite(oldSiteConfig),
            newPhpVersions: Set<PhpVersion> = this.getPhpVersionsToConfigureForSite(newSiteConfig);

        return oldPhpVersions.size !== newPhpVersions.size
            || ![...oldPhpVersions].every((phpVersion: PhpVersion): boolean => newPhpVersions.has(phpVersion));
    }

    public getRemovedPhpVersions(oldSiteConfig: SiteConfig, newSiteConfig: SiteConfig): Set<PhpVersion> {
        const oldSitePhpVersions: Set<PhpVersion> = this.getPhpVersionsToConfigureForSite(oldSiteConfig),
            newSitePhpVersions: Set<PhpVersion> = this.getPhpVersionsToConfigureForSite(newSiteConfig);

        return new Set<PhpVersion>([...oldSitePhpVersions].filter(
            (phpVersion: PhpVersion): boolean => !newSitePhpVersions.has(phpVersion)
        ));
    }
}
