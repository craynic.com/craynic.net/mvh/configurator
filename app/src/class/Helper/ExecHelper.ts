import * as child_process from "child_process";
import {ExecException} from "child_process";
import {Logger} from "../Logger";
import {singleton} from "tsyringe";

@singleton()
export class ExecHelper {
    public constructor(
        protected readonly logger: Logger,
    ) {
    }

    public execute(command: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            child_process.exec(command, (error: ExecException | null, stdout: string, stderr: string) => {
                if (error) {
                    reject(stderr);
                } else {
                    resolve(stdout);
                }
            });
        });
    }
}
