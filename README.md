# Multi-VirtualHosting - Configurator

Prepares a configuration for:
* Apache 2.4 VirtualHosts
* PHP Pool configuration files
* SFTP keys and password
* Home directories of the sites with XFS quotas enabled
* passwd-style files with all site owner-users

Initializes the volume:
* Creates directories for the virtual hosts
* Sets quotas for the directories

## Usage

Parameters can be set either as arguments or as ENV variables.

* `--mongo-conn-uri <string>`
    Connection URI to the MongoDB holding sites configuration [or env: `APP_MONGO_CONN_URI`]
* `--mongo-db <string>`
    MongoDB database holding sites configuration [or env: `APP_MONGO_DB`]
* `--apache-sites-dir <string>`
    The output directory for Apache configuration files [or env: `APP_APACHE_SITES_DIR`];  
    defaults to `"/app/data/apache-sites"` 
* `--php-pool-dir <string>`  
    The output directory for PHP configuration files [or env: `APP_PHP_POOL_DIR`];  
    defaults to `"/app/data/php-pool"`            
* `--php-pool-sockets-dir <string>`  
    The output directory for PHP pool sockets [or env: `APP_PHP_POOL_SOCKETS_DIR`];  
    defaults to `"/app/data/php-pool-sockets"`
* `--php-pool-sockets-type <unix|tcp>`  
    The type of sockets to use for Apache->PHP communication [or env: `APP_PHP_POOL_SOCKETS_TYPE`];  
    defaults to `"unix"`
* `--mounted-sites-root-dir <string>`  
    The mounted (current) root directory of all sites [or env: `APP_MOUNTED_SITES_ROOT_DIR`];  
    defaults to `"/app/data/sites-root"`
* `--target-sites-root-dir <string>`  
    The target (production) root directory of all sites [or env: `APP_TARGET_SITES_ROOT_DIR`];  
    defaults to `"/var/www"`
* `--sftp-keys-dir <string>`  
    The output directory holding all SFTP keys per user [or env: `APP_SFTP_KEYS_DIR`];
    defaults to `"/app/data/sftp-keys/"`                
* `--ssl-certs-dir <string>`
    The output directory holding all SSL certificates & keys [or env: `APP_SSL_CERTS_DIR`];
    defaults to `"/app/data/ssl-certs/"`
* `--apache-gid <number>`
    The GID of the Apache user [or env: `APP_APACHE_GID`];  
    defaults to `33`                                                     
* `--watch <true|false>`
    The flag indicating to run in watch (infinite) mode [or env: `APP_WATCH`];  
    defaults to `false`
* `--users-dir <string>`  
    The output directory holding the "passwd" file with all users [or env: `APP_USERS_DIR`];
    defaults to `"/app/data/users/"`
* `--k8s-certificate-issuer <Kind:Name>`
    The K8s certificate issuer in the format "Kind:Name", e.g. "ClusterIssuer:cert-issuer"
* `--apache-enabled-ports <string>`
    Enabled ports in Apache; comma-separated list of values, the options are:
    http (port 10080), https (port 10443), http-proxy (port 20080), https-proxy (port 20443);
    the *-proxy protocols have the PROXY protocol enabled [or env: `APP_APACHE_ENABLED_PORTS`];
    defaults to `"http, https"`
* `--apache-default-site <string>`
    Default site (VHost ServerName) for Apache [or env: `APP_APACHE_DEFAULT_SITE`]; 
    defaults to `""`
* `--php-version-header-name <string>`
    Name of the HTTP header to use to control PHP version used for multi-version PHP sites
    [or env: `APP_PHP_VERSION_HEADER_NAME`]; 
    no default value
* `--target-namespace <string>`
    The K8s namespace where the certificates are to be created [or env: `APP_TARGET_NAMESPACE`];
    defaults to `""`

You can run the container with `docker-compose` like this:

```shell script
docker-compose build && docker-compose run app \
    --mongo-conn-uri "mongodb://user:password@mongo:27017/db" \
    --mongo-db db \
    --apache-sites-dir /opt/test-output/sites-available \
    --mounted-sites-root-dir /opt/test-output/volume \
    --target-sites-root-dir /var/www \
    --sftp-keys-dir /opt/test-output/sftp-keys \
    --apache-gid 33 \
    --php-pool-dir /opt/test-output/php-pool.d \
    --php-pool-sockets-dir /opt/test-output/php-pool-sockets \
    --php-pool-sockets-type unix \
    --users-dir /opt/test-output/users \
    --k8s-certificate-issuer ClusterIssuer:cert-issuer \
    --apache-enabled-ports "http, https" \
    --apache-default-site "example.com" \
    --php-version-header-name "CraynicNet-PHP-Version" \
    --target-namespace "localdev"
```

or simply, when using the defaults or environment variables:

```shell script
docker-compose build && docker-compose run app
```

## Development Environment

During development, you'll probably prefer running it locally without Docker
to speed up the run.

The `yarn` command to build the TypeScript sources and run it is:

```shell script
yarn --cwd app main \
  --mongo-conn-uri "mongodb://user:password@mongo:27017/db" \
  --mongo-db db \
  --apache-sites-dir /opt/test-output/sites-available \
  --mounted-sites-root-dir /opt/test-output/volume \
  --target-sites-root-dir /var/www \
  --sftp-keys-dir /opt/test-output/sftp-keys \
  --apache-gid 33 \
  --php-pool-dir /opt/test-output/php-pool.d \
  --php-pool-sockets-dir /opt/test-output/php-pool-sockets \
  --php-pool-sockets-type unix \
  --users-dir /opt/test-output/users \
  --k8s-certificate-issuer ClusterIssuer:cert-issuer \
  --apache-enabled-ports "http, https" \
  --apache-default-site "example.com" \
  --php-version-header-name "CraynicNet-PHP-Version" \
  --target-namespace "localdev"
```

With defaults or environment variables:

```shell script
yarn --cwd app main
```
