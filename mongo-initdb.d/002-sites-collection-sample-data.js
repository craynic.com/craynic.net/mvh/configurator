// noinspection JSUnresolvedFunction,JSUnresolvedVariable

db = new Mongo().getDB("db");

db.sites.insertMany(
  [
    {
      "id": NumberInt('10001'),
      "name": "example.com",
      "username": "www_examplec",
      "quotaInGB": NumberInt('1'),
      "aliases": [
        "www.example.com"
      ],
      "php": {
        "version": "8.1",
        "request_timeout": NumberInt('30'),
        "pm": {
          "max_children": NumberInt('5'),
        },
        "memory_limit": "16M",
        "env": {
          "ENV_VARIABLE": "ENV_VALUE",
          "ENV_VARIABLE2": "ENV_VALUE2"
        }
      },
      "sftp": {
        "keys": [
          "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDiZtLaMPyVVHE/sgWTmMOO1C0KBPXD8NHydR37jwyeovnksHxtYpxjzYW3valJpQSZiUtonEXCwwJOeEvjGRbjglrOwx5E8rEwPp9QbezYPd4MV8vJJre814N/o95fgr8QbnHWFK140C1MY2938ScVxMdEi0rLu5Fh1JbS+HYXk1zagTCytYJvv9Cpvxe9ckZC2m1RP8zO3Rlu4BEuvm9Hwvh+TzH7mmjDMWB3eGBtq/XF/tCPggWiOj4MR+EId6eHVF6p3X+WYUKOpO9tJdH7B6zilL3eM2jcIzkluAtifgmjEYldsytW2H4hM2TymfHdPnSkhtjEQNLKUkbyi2og2pzJilbLm8LAKy/Oe2kX6qI1DYdA+SPpOD5uORhO8pJeDcLHJreYqrJTnFzxMG9ZjWsP0Xi3+cBa6LiFW+rwESP5saXZ+hvCSJAVKyw8yi/QdwCzDT4GKocVz+1PDJ0HQfNjmYBOrwFOm5b9/wyn/ikEs9PsjF8S7q8qv7+Yjfs= test",
        ]
      }
    },
    {
      "id": NumberInt('10002'),
      "name": "another-example.org",
      "username": "www_anothere",
      "quotaInGB": NumberInt('5'),
      "aliases": [
        "www.another-example.org",
        "www2.another-example.org",
      ],
      "php": {
        "version": "7.4",
        "request_timeout": NumberInt('30'),
        "pm": {
          "max_children": NumberInt('5'),
        },
        "memory_limit": "16M"
      },
      "sftp": {
        "keys": [
        ]
      }
    },
  ]
);
