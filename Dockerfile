# 1. build the dist
# !!! Node version 20 does NOT work !!!
FROM node:22-alpine@sha256:9bef0ef1e268f60627da9ba7d7605e8831d5b56ad07487d24d1aa386336d1944 AS builder

WORKDIR /app
COPY /app/src ./src
COPY /app/package.json /app/tsconfig.json /app/yarn.lock ./
RUN yarn --ignore-optional && yarn tsc

# 2. build the production container
# !!! Node version 20 does NOT work !!!
FROM node:22-alpine@sha256:9bef0ef1e268f60627da9ba7d7605e8831d5b56ad07487d24d1aa386336d1944 AS production

RUN apk --no-cache add \
        xfsprogs~=6 \
        xfsprogs-extra~=6 \
        acl~=2 \
        openssl~=3 \
        bash~=5 \
    && apk upgrade --no-cache \
        # various CVEs fixed in 3.0.8
        libssl3 libcrypto3

ENV APP_LOG_LEVEL="" \
    APP_MONGO_CONN_URI="" \
    APP_MONGO_DB="" \
    APP_APACHE_SITES_DIR="" \
    APP_MOUNTED_SITES_ROOT_DIR="" \
    APP_TARGET_SITES_ROOT_DIR="" \
    APP_PHP_POOL_DIR="" \
    APP_PHP_POOL_SOCKETS_DIR="" \
    APP_PHP_POOL_SOCKETS_TYPE="unix" \
    APP_SFTP_KEYS_DIR="" \
    APP_APACHE_GID="" \
    APP_WATCH="false" \
    APP_SSL_CERTS_DIR="" \
    APP_USERS_DIR="" \
    APP_K8S_CERTIFICATE_ISSUER="" \
    APP_APACHE_ENABLED_PORTS="http, https" \
    APP_APACHE_DEFAULT_SITE="" \
    APP_PHP_VERSION_HEADER_NAME="" \
    APP_TARGET_NAMESPACE="" \
    KUBECONFIG=""

WORKDIR /app
COPY --from=builder /app/dist ./dist
COPY /app/data ./data
COPY /app/package.json /app/yarn.lock ./

COPY files/ /

RUN yarn --ignore-optional --production

CMD ["node", "dist/main.js"]

# 3. build the local development container
FROM production AS development

COPY /app/src ./src
COPY /app/tsconfig.json ./

RUN yarn --ignore-optional

CMD ["yarn", "main"]
